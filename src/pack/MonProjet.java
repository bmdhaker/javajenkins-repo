package pack;

/* inverse d'une suite de caractère dans un tableau par
permutation des deux extrêmes */
public class MonProjet

{
	public String inverser(char[] original) {
		
		int i, j;
		System.out.println("tableau avant : " + String.valueOf(original));
		for (i = 0, j = 5; i < j; i++, j--) {
			char car;
			car = original[i];
			original[i] = original[j];
			original[j] = car;
		}
		//System.out.println("tableau après : " + String.valueOf(original));
		return String.valueOf(original);
	}
	
	

	public static void main(String[] args) {
/*		char[] Tablecar = { 'a', 'b', 'c', 'd', 'e', 'f' };
		int i, j;
		System.out.println("tableau avant : " + String.valueOf(Tablecar));
		for (i = 0, j = 5; i < j; i++, j--) {
			char car;
			car = Tablecar[i];
			Tablecar[i] = Tablecar[j];
			Tablecar[j] = car;
		}
		System.out.println("tableau après : " + String.valueOf(Tablecar));
*/
		char[] Tablecar = { 'd', 'h', 'a', 'k', 'e', 'r' };
		MonProjet p=new MonProjet();
		System.out.println("tableau après : " + p.inverser(Tablecar));
	}
		
}
