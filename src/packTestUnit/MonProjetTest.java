package packTestUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import pack.MonProjet;

public class MonProjetTest {

	@Test
	public void testInverser() {
		char[] original = { 'a', 'b', 'c', 'd', 'e', 'f' };
		MonProjet m = new MonProjet();
		// fail("Not yet implemented");
		assertTrue(m.inverser(original).equals("fedcba"));
	}

}
